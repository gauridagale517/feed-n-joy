/*
 * Author name : Gauri Dagale
 * Description : Contact Us Form
 * Input : Details
 * Output : Saving of details
 */

 package com.stories.JavaFiles;

 import java.io.FileInputStream;
import java.io.IOException;

import javafx.application.Application;
 import javafx.event.ActionEvent;
 import javafx.event.EventHandler;
 import javafx.geometry.Insets;
 import javafx.geometry.Pos;
 import javafx.scene.Group;
 import javafx.scene.Scene;
 import javafx.scene.control.Button;
 import javafx.scene.control.Label;
 import javafx.scene.control.TextArea;
 import javafx.scene.control.TextField;
 import javafx.scene.image.Image;
 import javafx.scene.image.ImageView;
 import javafx.scene.layout.BorderPane;
 import javafx.scene.layout.HBox;
 import javafx.scene.layout.StackPane;
 import javafx.scene.layout.VBox;
 import javafx.scene.paint.Color;
 import javafx.scene.text.Font;
 import javafx.scene.text.FontWeight;
 import javafx.stage.Stage;
 
 public class contactUs extends Application{
 
     @Override
     public void start(Stage prStage) throws IOException  {
 
         // image for background

         FileInputStream fxd_FileInputStream = new FileInputStream("ourstories/src/main/java/com/stories/Assets/Images/gradient.jpg");
 
         Image fxd_image2 = new Image(fxd_FileInputStream);
         ImageView fxd_imageView2  = new ImageView(fxd_image2);
         //fxd_imageView2.setOpacity(0.8);
         fxd_imageView2.setFitHeight(2600);
         fxd_imageView2.setFitWidth(2600);
         fxd_imageView2.setPreserveRatio(true);
 
         // Label for heading (Contact Us)
 
         Label fxd_label = new Label("Contact Us");
         fxd_label.setFont(Font.font("comic sanc",FontWeight.BOLD,40));
         fxd_label.setTextFill(Color.BLACK);
         fxd_label.setPadding(new Insets(0,0,0,0));
 
         // label for Email address
 
         Label fxd_label2 = new Label("Email address : ");
         fxd_label2.setFont(Font.font("comic sanc",FontWeight.SEMI_BOLD,20));
         fxd_label2.setTextFill(Color.BLACK);
         fxd_label2.setPadding(new Insets(0,680,0,0));
 
 
         Label fxd_label3 = new Label("We'll never share your email with anyone else");
         fxd_label3.setFont(Font.font("comic sanc",13));
         fxd_label3.setTextFill(Color.BLACK);
         fxd_label3.setPadding(new Insets(0,540,0,0));
 
         // Label for Username
 
         Label fxd_label4 = new Label("Enter username : ");
         fxd_label4.setFont(Font.font("comic sanc",FontWeight.SEMI_BOLD,20));
         fxd_label4.setTextFill(Color.BLACK);
         fxd_label4.setPadding(new Insets(0,660,0,0));
 
         Label fxd_label5 = new Label("We'll require username for unique identity");
         fxd_label5.setFont(Font.font("comic sanc",13));
         fxd_label5.setTextFill(Color.BLACK);
         fxd_label5.setPadding(new Insets(0,550,0,0));
 
         // Label for Password
 
         Label fxd_label6 = new Label("Password : ");
         fxd_label6.setFont(Font.font("comic sanc",FontWeight.SEMI_BOLD,20));
         fxd_label6.setTextFill(Color.BLACK);
         fxd_label6.setPadding(new Insets(0,710,0,0));
 
         // Label for State
 
         Label fxd_label7 = new Label("Enter State : ");
         fxd_label7.setFont(Font.font("comic sanc",FontWeight.SEMI_BOLD,20));
         fxd_label7.setTextFill(Color.BLACK);
         fxd_label7.setPadding(new Insets(0,700,0,0));
 
         // Label for City
 
         Label fxd_label8 = new Label("Enter city : ");
         fxd_label8.setFont(Font.font("comic sanc",FontWeight.SEMI_BOLD,20));
         fxd_label8.setTextFill(Color.BLACK);
         fxd_label8.setPadding(new Insets(0,710,0,0));
 
         // Label for Feedback
 
         Label fxd_label9 = new Label("Feedback : ");
         fxd_label9.setFont(Font.font("comic sanc",FontWeight.SEMI_BOLD,20));
         fxd_label9.setTextFill(Color.BLACK);
         fxd_label9.setPadding(new Insets(0,710,0,0));
 
         // textfield for enter Email address
 
         TextField fxd_textField = new TextField();
         fxd_textField.setMaxWidth(800);
 
         // textfield for enter Username
 
         TextField fxd_textField2 = new TextField();
         fxd_textField2.setMaxWidth(800);
 
         // textfield for enter Password
 
         TextField fxd_textField3 = new TextField();
         fxd_textField3.setMaxWidth(800);
 
         // textfield for enter State
 
         TextField fxd_textField4 = new TextField();
         fxd_textField4.setMaxWidth(800);
 
         // textfield for enter City
 
         TextField fxd_textField5 = new TextField();
         fxd_textField5.setMaxWidth(800);
 
         // textarea for enter Feedback
 
         TextArea fxd_textArea = new TextArea();
         fxd_textArea.setMaxSize(800,30);
 
         // button for submit details
 
         Button fxd_button = new Button("submit");
         fxd_button.setMaxHeight(20);
         fxd_button.setMaxWidth(100);
         fxd_button.setFont(Font.font("comic sanc",FontWeight.BOLD,15));
         fxd_button.setTextFill(Color.rgb(0, 150, 255));
         fxd_button.setStyle("-fx-padding : 8; -fx-background-radius:20");
         fxd_button.setAlignment(Pos.CENTER);
         fxd_button.setPadding(new Insets(50, 0, 0, 0));
 
         // Hbox for navbar    
         HBox fxd_navbar = new HBox();
         fxd_navbar.setSpacing(40);
         fxd_navbar.setPrefHeight(70);
         fxd_navbar.setPadding(new Insets(0, 70, 70, 0));
         fxd_navbar.setAlignment(Pos.CENTER);
 
         // image for adding logo 
 
         FileInputStream fxd_FileInputStream2 = new FileInputStream("ourstories\\src\\main\\java\\com\\stories\\Assets\\Images\\hlogo.png");
         
         Image fxd_image = new Image(fxd_FileInputStream2);
         ImageView fxd_imageView = new ImageView(fxd_image);
         fxd_imageView.setFitHeight(200);
         fxd_imageView.setFitWidth(300);
         fxd_imageView.setPreserveRatio(true);
 
 
         // Buttons for navbar
 
         Button fxd_homeButton = createStyledButton("Home");
         Button fxd_aboutButton = createStyledButton("About");
         Button fxd_contactButton = createStyledButton("Contact");
         Button fxd_registerButton = createStyledButton("Register");
         Button fxd_storiesButton = createStyledButton("Stories");
 
     //add all buttons to HBox    
         fxd_navbar.getChildren().addAll(fxd_imageView,fxd_homeButton, fxd_aboutButton, fxd_registerButton, fxd_contactButton,fxd_storiesButton);
         
         Label fxd_content = new Label("");
 
     // display inner information of button
         fxd_homeButton.setOnAction(new EventHandler<ActionEvent>() {
             @Override   
             public void handle(ActionEvent event) {
                fxd_content.setText("homepage");
             }    
         });
         fxd_aboutButton.setOnAction(new EventHandler<ActionEvent>() {
             @Override
             public void handle(ActionEvent event) {
                fxd_content.setText(" About page");
             }    
         });
         fxd_registerButton.setOnAction(new EventHandler<ActionEvent>() {
             @Override
             public void handle(ActionEvent event) {
                fxd_content.setText(" Register as");
             }    
         });
         fxd_contactButton.setOnAction(new EventHandler<ActionEvent>() {
             @Override
             public void handle(ActionEvent event) {
                fxd_content.setText("contact us");
             }    
         });
         fxd_storiesButton.setOnAction(new EventHandler<ActionEvent>() {
             @Override
             public void handle(ActionEvent event) {
                fxd_content.setText(" stories");
             }    
         });
 
         // HBox for aligining navbar
 
         HBox fxd_hb = new HBox(fxd_navbar);
         fxd_hb.setLayoutX(250);
         fxd_hb.setLayoutY(130);
         
         // VBox to align email address, username, password, state, city, feedback vertically
 
         VBox fxd_vbox = new VBox(12,fxd_label,fxd_label2,fxd_textField,fxd_label3,fxd_label4,fxd_textField2,fxd_label5,fxd_label6,fxd_textField3,fxd_label7,fxd_textField4,fxd_label8,fxd_textField5,fxd_label9,fxd_textArea);
         fxd_vbox.setAlignment(Pos.CENTER);
         VBox fxd_vb1Button = new VBox(fxd_button);
         fxd_vb1Button.setAlignment(Pos.CENTER);
 
         VBox fxd_BPpass = new VBox( 30,fxd_vbox,fxd_vb1Button);
         
         // borderpane for alignment of HBox and VBox
 
         BorderPane fxd_borderPane = new BorderPane();
         fxd_borderPane.setTop(fxd_hb);
         fxd_borderPane.setCenter(fxd_BPpass);
         
         Group fxd_gp = new Group(fxd_borderPane);
         fxd_gp.setLayoutY(23);
         fxd_gp.setLayoutX(450);
 
         // stackpane for adding background image
 
         StackPane fxd_stackPane = new StackPane(fxd_imageView2,fxd_gp);
 
         Scene sc = new Scene(fxd_stackPane,1000,1000);
         prStage.setScene(sc);
         prStage.show();         
     }
 
     // for styling buttons
 
     public Button createStyledButton(String text) {
         Button button2 = new Button(text);
 
         button2.setPrefHeight(50);
         button2.setPrefWidth(120);
         button2.setStyle("-fx-background-color:GRAY; -fx-text-fill: white; -fx-font-size: 20px; -fx-font-weight: bold; -fx-background-radius: 35;");
         button2.setOnMouseEntered(e -> button2.setStyle("-fx-background-color:BLACK; -fx-text-fill: white; -fx-font-size: 20px; -fx-font-weight: bold; -fx-background-radius: 20;"));
         button2.setOnMouseExited(e -> button2.setStyle("-fx-background-color:GRAY; -fx-text-fill: white; -fx-font-size: 20px; -fx-font-weight: bold; -fx-background-radius: 20;"));
         return button2;
 }
     
 }
 